// Tutoral Url: http://onlinehelp.tableau.com/samples/en-us/js_api/tutorial.htm

var viz, workbook, activeSheet, selectedItem, vizFilters, vizFilters2, vizSelected, filters2 = [], filters = [];

var viz2, workbook2, activeSheet2;

function initializeViz() {
    var vizDiv = document.getElementById("app-visit");
    var url = "https://analytics.fantain.com/#/site/FantainCustomer/views/Campaign/Hits?:iid=34";
    var options = {
        hideTabs: true,
        hideToolbar: true,
        onFirstInteractive: function () {
            workbook = viz.getWorkbook();
            activeSheet = workbook.getActiveSheet();

            // Get Filters
            activeSheet.getFiltersAsync()
                .then(function(data) {
                    console.log(data);
                    vizFilters = data;

                   getFilters(vizFilters, filters);

                    var filtersDiv = $('#sidebar');

                    setFiltersToDom(filtersDiv, filters);

                })
                .otherwise(function(err) {
                    console.log('Error: ' + err);
                });

            //update all the value

        }

    };
    viz = new tableau.Viz(vizDiv, url, options);
}

function initializeViz2() {
    var vizDiv = document.getElementById("referred-from");
    var url = "https://analytics.fantain.com/#/site/DevFantainCustomer/views/Campaign/MailsSent";
    var options = {
        hideTabs: true,
        hideToolbar: true,
        onFirstInteractive: function () {
            workbook2 = viz2.getWorkbook();
            activeSheet2 = workbook2.getActiveSheet();

            // Get Filters
            activeSheet2.getFiltersAsync()
                .then(function(data) {
                    console.log('Referred-filter: ' + data);
                    vizFilters2 = data;

                    getFilters(vizFilters2, filters2);

                    var filtersDiv = $('#sidebar');

                    setFiltersToDom(filtersDiv, filters2);


                })
                .otherwise(function(err) {
                    console.log('Error: ' + err);
                });

            // Get Selections
            activeSheet2.getSelectedMarksAsync()
                .then(function(data) {
                    console.log('Referred-selection: ' + data);
                    vizSelected = data;
                })
                .otherwise(function(err) {
                    console.log('Error: ' + err);
                });
        }
    };
    viz2 = new tableau.Viz(vizDiv, url, options);
}

// Get filters
function getFilters(arr, outArr) {
    var arrLen = arr.length,
        filterFieldsIn,
        filterFieldsInLen,
        filterFieldsOut = [],
        filterField,
        item,
        i,
        j,
        obj = {};

    // get Each filters title and fields
    for(i = 0; i < arrLen; i += 1) {
        obj = {}, filterFields = [];

        item = arr[i];
        filterFieldsIn = item['$B'];
        filterFieldsInLen = filterFieldsIn.length;

        obj.title = item.$2;

        for(j = 0; j < filterFieldsInLen; j += 1) {
            filterField = filterFieldsIn[j].formattedValue;
            filterFieldsOut.push(filterField);
        }

        obj.fields = filterFieldsOut;

        outArr.push(obj);
    }
}

// Set filters
function setFiltersToDom($parent, arr) {
    var arrLen = arr.length,
        i,
        item,
        itemFields,
        itemField,
        itemFieldsLen,
        j,
        filterHtml,
        li,
        o,
        filterItem;

    for(i = 0; i < arrLen; i += 1) {
        filterHtml = '';

        item = arr[i];
        itemFields = item.fields;
        itemFieldsLen = itemFields.length;

        filterHtml += '<h2>' + item.title +'</h2>';

        filterHtml += '<ul>';

        filterHtml += '<li class="filter-item all">All</li>';

        filterHtml += '<li class="filter-item Select">Select</li>'

        for(j = 0; j < itemFieldsLen; j += 1) {
            itemField = itemFields[j];

            li = '<li class="filter-item">'+ itemField + '</li>';

            filterHtml += li;


        }

        $parent.append(filterHtml);

        filterItem = $('.filter-item');

        filterItem.on('click', function() {
            o = $(this);
            filterItem.css('opacity', 0.5);
            o.css('opacity', 1);

            if(!o.hasClass('all')) {
                filterFun(activeSheet2, item.title, o.text());
            } else {
                filterFun(activeSheet2, item.title, '');
            }
        });
    }
}

// Api filter
function filterFun(activeSheet, category, filterItem) {
    if(filterItem != '') {
        activeSheet.applyFilterAsync(
            category,
            filterItem,
            tableau.FilterUpdateType.REPLACE
        )
    } else {
        activeSheet.applyFilterAsync(
            category,
            filterItem,
            tableau.FilterUpdateType.ALL
        )
    }
}




$(document).ready(function() {
    initializeViz();
    initializeViz2();
});