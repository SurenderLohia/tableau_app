// Tutoral Url: http://onlinehelp.tableau.com/samples/en-us/js_api/tutorial.htm

var viz, workbook, activeSheet, selectedItem, fields = [];

var viz2, workbook2, activeSheet2;

function initializeViz() {
    var vizDiv = document.getElementById("viz");
    var url = "http://public.tableau.com/views/WorldIndicators/GDPpercapita";
    var options = {
        hideTabs: true,
        hideToolbar: true,
        onFirstInteractive: function () {
            workbook = viz.getWorkbook();
            activeSheet = workbook.getActiveSheet();

            /*activeSheet.getDataSourcesAsync()
                .then(function(data) {
                    fields = data[0].getFields();
                    console.log(fields);
                })
                .otherwise(function(err) {
                    console.log('Error: ' + err);
                })*/

            // Get Filters
            activeSheet.getFiltersAsync()
                .then(function(data) {
                    //fields = data[0].getFields();
                    console.log(data);
                })
                .otherwise(function(err) {
                    console.log('Error: ' + err);
                })

            // Set size
            activeSheet.changeSizeAsync({
                behavior: tableau.SheetSizeBehavior.AUTOMATIC
            });
        }
    };
    viz = new tableau.Viz(vizDiv, url, options);
}

function initializeViz2() {
    var vizDiv = document.getElementById("map");
    var url = "http://public.tableau.com/views/WorldIndicators/GDPpercapitamap";
    var options = {
        hideTabs: true,
        hideToolbar: true,
        onFirstInteractive: function () {
            workbook2 = viz2.getWorkbook();
            activeSheet2 = workbook2.getActiveSheet();
        }
    };
    viz2 = new tableau.Viz(vizDiv, url, options);
}

// Filter
function filterSingleValue() {
    activeSheet.applyFilterAsync(
        'Region',
        'The Americas',
        tableau.FilterUpdateType.REPLACE
    )
}

function addValuesToFilter() {
    activeSheet.applyFilterAsync(
        'Region',
        ['Europe', 'Middle East'],
        tableau.FilterUpdateType.ADD
    )
}

function removeValuesFromFilter() {
    activeSheet.applyFilterAsync(
        'Region',
        'Europe',
        tableau.FilterUpdateType.REMOVE
    )
}

function filterRangeOfValues() {
    activeSheet.applyRangeFilterAsync(
        'F: GDP per capita (curr $)',
        {
            min: 40000,
            max: 60000
        },
        tableau.FilterUpdateType.REPLACE
    )
}

function clearFilters() {
    activeSheet.clearFilterAsync('Region');
    activeSheet.clearFilterAsync('F: GDP per capita (curr $)');
}

// Switch view
function switchToMapTab() {
    workbook.activateSheetAsync('GDP per capita map');
}

// Select
function selectSingleValue() {
    workbook.getActiveSheet().selectMarksAsync(
        'Region',
        'Asia',
        tableau.SelectionUpdateType.REPLACE
    )
}

function addValuesToSelection() {
    workbook.getActiveSheet().selectMarksAsync(
        'Region',
        ['Africa', 'Oceania'],
        tableau.SelectionUpdateType.ADD
    )
}

function removeFromSelection() {
    workbook.getActiveSheet().selectMarksAsync(
        'AVG(F: GDP per capita (curr $))',
        {
            min: 5000
        },
        tableau.SelectionUpdateType.REMOVE
    )
}

function clearSelection() {
    workbook.getActiveSheet().clearSelectedMarksAsync();
}

// Chain calls
function switchFilterSelect() {
    workbook.activateSheetAsync('GDP per capita by region')
        .then(function(newSheet) {
            activeSheet = newSheet;

            return activeSheet.applyRangeFilterAsync(
                'Date (year)',
                {
                    min: new Date(Date.UTC(2002, 1, 1)),
                    max: new Date(Date.UTC(2008, 12, 31))
                },
                tableau.FilterUpdateType.REPLACE
            );
        })
        .otherwise(function(err) {
            console.log('Error: ' + err);
        })
        .then(function(filterFieldName) {
            return activeSheet.selectMarksAsync(
                'AGG(GDP per capita (weighted))',
                {
                    min: 20000
                },
                tableau.SelectionUpdateType.REPLACE
            );
        })
}



// Sheets
function querySheets() {
    var sheets = workbook.getPublishedSheetsInfo(),
        sheetsLen = sheets.length,
        text = '',
        i;

    for(i = 0; i < sheetsLen; i += 1) {
        text += 'Sheet ' + i + ' - ' + sheets[i].getName() + '\n';
    }

    alert(text);
}

function queryDashboard() {
    workbook.activateSheetAsync('GDP per Capita Dashboard')
        .then(function(dashboard) {
            var sheets = dashboard.getWorksheets(),
                sheetsLen = sheets.length,
                text = '',
                i;

            for(i = 0; i < sheetsLen; i += 1) {
                text += 'Sheet ' + i + ' - ' + sheets[i].getName() + '\n';
            }

            alert(text);
        })
        .otherwise(function(err) {
            console.log('Error: ' + err);
        })
}

function multipleSheets() {
    var dashboard, mapSheet, graphSheet;

    workbook.activateSheetAsync('GDP per Capita Dashboard')
        .then(function(sheet) {
            dashboard = sheet;

            // Set size
            dashboard.changeSizeAsync({
                behavior: tableau.SheetSizeBehavior.AUTOMATIC
            });

            mapSheet = dashboard.getWorksheets().get('Map of GDP per capita');
            graphSheet = dashboard.getWorksheets().get('GDP per capita by region');
            return mapSheet.applyFilterAsync(
                'Region',
                'Middle East',
                tableau.FilterUpdateType.REPLACE
            )
        })
        .otherwise(function(err) {
            console.log('Error: ' + err);
        })
        .then(function() {
            mapSheet.applyFilterAsync(
                'YEAR(Date (year))',
                2010,
                tableau.FilterUpdateType.REPLACE
            )
        })
        .otherwise(function(err) {
            console.log('Error: ' + err);
        })
        .then(function() {
            return graphSheet.selectMarksAsync(
                'YEAR(Date (year))',
                2009,
                tableau.SelectionUpdateType.REPLACE
            )
        })
        .otherwise(function(err) {
            console.log('Error: ' + err);
        })
}

// Toolbar
function exportPDF() {
    viz.showExportPDFDialog();
}

function exportImg() {
    viz.showExportImageDialog();
}

function exportCrossTab() {
    viz.showExportCrossTabDialog();
}

function exportData() {
    viz.showExportDataDialog();
}

function revertAll() {
    workbook.revertAllAsync();
}



function multipleSheets2() {
    var dashboard, mapSheet, graphSeet;

    workbook.activateSheetAsync('GDP per Capita Dashboard')
        .then(function(sheet) {
            dashboard = sheet;

            // set size
            dashboard.changeSizeAsync({
                behavior: tableau.SheetSizeBehavior.AUTOMATIC
            })
        })
        .otherwise(function(err) {
            console.log('Error: ' + err);
        })
}


// Multi sheet filter
function multipleFilter() {
    var dashboard, mapSheet, graphSheet;

    dashboard = workbook.getActiveSheet();

    mapSheet = dashboard.getWorksheets().get('Map of GDP per capita');
    graphSheet = dashboard.getWorksheets().get('GDP per capita by region');

    // map sheet select
    var mapFilter = mapSheet.selectMarksAsync(
        'Region',
        'Europe',
        tableau.SelectionUpdateType.REPLACE
    );

    console.log('mapFilter: ' + mapSheet);
}

// Events
function listenToMarksSelection() {
    viz.addEventListener(tableau.TableauEventName.MARKS_SELECTION, onMarksSelection);
}

function onMarksSelection(marksEvent) {
    return marksEvent.getMarksAsync().then(reportSelectedMarks);
}

function reportSelectedMarks(marks) {

    console.log(marks);

    var html = [];

    selectedItem = [];

    for (var markIndex = 0; markIndex < marks.length; markIndex++) {
        var pairs = marks[markIndex].getPairs();

        html.push("Mark " + markIndex + ":");
        for (var pairIndex = 0; pairIndex < pairs.length; pairIndex++) {
            var pair = pairs[pairIndex];

            if(pair.fieldName === 'Region') {
                selectedItem.push(pair.formattedValue);
            }

            html.push("fieldName: " + pair.fieldName + '\n');
            html.push("formattedValue: " + pair.formattedValue + "\n");

        }
    }

    console.log('Selected Item: ' + selectedItem);
    console.log(html);



    // Selected Filter
    activeSheet2.applyFilterAsync(
        'Region',
        selectedItem,
        tableau.FilterUpdateType.REPLACE
    )
}

function removeMarksSelectionEventListener() {
    viz.removeEventListener(tableau.TableauEventName.MARKS_SELECTION, onMarksSelection);
}


$(document).ready(function() {
    initializeViz();
    initializeViz2();
});